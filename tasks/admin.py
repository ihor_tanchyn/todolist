from __future__ import unicode_literals

from django.contrib import admin
from .models import Task

class TaskAdmin(admin.ModelAdmin):
    fields = ['name', 'author','created', 'details', 'done']
    list_display = ['id', 'name', 'author']
    list_filter = ['author']

admin.site.register(Task, TaskAdmin)
