from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User


class Task(models.Model):
    name = models.CharField(max_length=100);
    author = models.ForeignKey(User)
    details = models.TextField(blank=True)
    created = models.DateField(auto_now_add=True)
    done = models.BooleanField(default=False)

    def __unicode__(self):
        return self.name
